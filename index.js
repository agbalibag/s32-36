
//use directive keyword require to make a module useful in the current file/module
const express = require('express');

//express function is our server stored in a constant variable app
const app = express();

//require mongoose module to be used in our entry point file index.js
const mongoose = require('mongoose');
const cors = require('cors');

const PORT = 3001;

//MIDDLEWARES
//express.json is an express framework to parse incoming json payloads
app.use(express.json()) 
app.use (express.urlencoded({extended:true}));
app.use(cors());

//Connecting userRoutes module to index.js entry point
const userRoutes = require('./routes/userRoutes');

//Course Routes
const courseRoutes = require('./routes/courseRoutes');

//connect to mongoDB database
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.upmgf.mongodb.net/courseBooking?retryWrites=true&w=majority', 
	{useNewUrlParser: true, useUnifiedTopology: true}
);

//Notification
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));


//routes
	// http://localhost:3001/users

	//HTTP METHODS
		//GET
		//POST
		//PUT
		//DELETE
		//PATCH
/*
app.get('/users', (req, res) => {
	// console.log("hello")
	res.send('Hello')
})

app.post('/users', (req, res) => {
	// console.log(req.body) //contains object
	// console.log(req)
	let name = req.body.name


	res.send(`Hello ${name}`)
})

*/

//middleware entry point url (root url before any endpoints)
app.use("/api/users", userRoutes);

app.use("/api/courses", courseRoutes);

//server listening to port 3001
app.listen(PORT, () => console.log(`Server is running at ${PORT}`));
