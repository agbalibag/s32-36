
//json web token
	// sign -  function that creates a token
	// verify - function that checks if there's a presence of token
	// decode - function that decodes the token

const jwt = require('jsonwebtoken');
const secret = "dontTellAnyone"

module.exports.createAccessToken = (user) => {
	//user = object because it came from the matching document of the user from the database
	
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// jwt.sign(payload, secret, {});

	return jwt.sign(data, secret, {});
}

//decode token
module.exports.decode = (token) => {
	// console.log(token) //token
	//jwt.decode(token, {})

	let slicedToken = token.slice(7, token.length);
	console.log(slicedToken)

	return jwt.decode(slicedToken, {complete: true}).payload

}

//verify
module.exports.verify = (req, res, next) => {

	//get the token(headers)
	let token = req.headers.authorization
	

	// console.log(token)

	//jwt.verify(token, secret, function)
	if(typeof token !== "undefined"){

		let slicedToken = token.slice(7, token.length);


		return jwt.verify(slicedToken, secret, (err, data) => {
			if(err){
				res.send( {auth: "failed"})
			}else{
				next()
			}
		})
	}else{
		res.send(false)
	}
}

