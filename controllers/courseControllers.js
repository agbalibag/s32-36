const Course = require('../models/Course');

const auth = require('../auth');

const bcrypt = require ('bcrypt');

//Get all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		if(result !== null){
			return result;
		}else{
			return error
		}	
	})
}

//create a course
module.exports.createCourse = (reqBody) => {


	const newCourse = new Course({
		courseName: reqBody.courseName,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then( (result, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}


//get specific course using findById()
module.exports.getCourse = (courseId) => {
	//The "findById" Mongoose method will look for a task with the same id provided from the URL
	return Course.findById(courseId).then((result, error) => {
		if(error){
			console.log(error)
			return false;
			//Find successful, returns the task object back to the client/Postman
		}else{
			return result;
		}
	})
}


