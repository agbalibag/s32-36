//Business logic
	//including model methods

const User = require('./../models/User');
const Course = require('./../models/Course');

const auth = require('./../auth');

const bcrypt = require('bcrypt');

module.exports.checkEmail = (email) => {


	return User.findOne({email: email}).then( (result, error) => {
	console.log(result)//null
		if(result != null){
			return (false)
		}else{
			if(result == null){
				//send back the response to the client
				return (true)
			}else{
				return (error)
			}
		}
	})
}
//checkEmail(<arguments>)

// 	module.exports.register = (reqBody) => {
// 	//save/create a new user document
// 	console.log(reqBody)


// 	const {firstName, lastName, email, password, mobileNo, age} = reqBody
// 	// console.log(firstName)

// 	const newUser = new User({
// 		firstName: firstName,
// 		lastName: lastName,
// 		email: email,
// 		password: password,
// 		mobileNo: mobileNo,
// 		age: age
// 	})

// 	//save the newUser object to the database
// 	return newUser.save().then( (result, error) => {
// 		if(result){
// 			return true
// 		} else{
// 			return error
// 		}

// 	})
// }

module.exports.register = (reqBody) => {
	//save/create a new user document
		//using .save() method to save document to the database
		// console.log(reqBody)	//object user document

		//how to use object destructuring
			//why? to make distinct variables for each property w/o using dot notation
			// const {properties} = <object reference>
	const {firstName, lastName, email, password, mobileNo, age} = reqBody
	// console.log(firstName)

	const newUser = new User({
		firstName: firstName,
		lastName: lastName,
		email: email,
		password: bcrypt.hashSync(password, 10),
		mobileNo: mobileNo,
		age: age
	})

	//save the newUser object to  the database
	return newUser.save().then( (result, error) => {
		//console.log(result)	//document
		if(result){
			return true
		} else {
			return error
		}
	})
}


//Activity - get all users
module.exports.getAllUsers = () => {
	//difference between findOne() & find
    return User.find().then( (result, error) => {
    	if(result !== null){
    		return result
    	}else{
    		return error
    	}
    })
 }

//login
 module.exports.login = (reqBody) => {
 	const {email, password} = reqBody

 	return User.findOne({email: email}).then( (result, error) => {
 		// console.log(result)

 		if(result == null){
 			console.log('email null');
 			return false
 		}else{

 			 let isPwCorrect = bcrypt.compareSync(password, result.password)

 			if(isPwCorrect == true){
 			//return json web token
 				//invoke the function that creates the token upon logging in
 				//requirements in creating a token:
 					//if password matches from existing pw from database
 					return {access: auth.createAccessToken(result)}
 			}else{
 				return false
 			}
 		}
 	})
 }

/*mini activity*/	
	// using findById method, look for the matching document and return the matching document to the client

module.exports.getUserProfile = (reqBody)=>{
	return User.findById({_id: reqBody.id}).then(result => {
		return result;
	})
}

//edit user informattion
module.exports.editProfile = (userId, reqBody)=> {
	// console.log(userId)
	// console.log(reqBody)

	return User.findByIdAndUpdate(userId, reqBody, {new:true}).then( result => {
		// console.log(result)

		result.password="****"
		return result
	})
}

module.exports.editUser = (userId, reqBody) => {
	// console.log(userId)
	// console.log(reqBody)
	const {firstName, lastName, email, password, mobileNo, age} = reqBody
	// console.log(firstName)
	// return User.findByIdAndUpdate(userId, )

	const updatedUser = {
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobileNo: mobileNo,
		age: age,
		password: bcrypt.hashSync(password, 10)
	}

	return User.findByIdAndUpdate(userId, updatedUser, {new:true}).then( result => {

		result.password ="****"
		// console.log(result)
		return result
	})
}

//mini ctivity

module.exports.editDetails = (reqBody) => {
	// console.log(reqBody)
	const {firstName, lastName, email, password, mobileNo, age} = reqBody

	const updatedUser = {
		firstName: firstName,
		lastName: lastName,
		email: email,
		mobileNo: mobileNo,
		age: age,
		password: bcrypt.hashSync(password, 10)
	}


	return User.findOneAndUpdate({email: email}, updatedUser, {returnDocument:'after'}).then( result => {
		result.password = "****"

		return result
	})
}


//delete
module.exports.delete = (userId) => {

	return User.findByIdAndDelete(userId).then( result => {
		// console.log(result)
		if(result){
			return true
		}
	})
}

module.exports.deleteUser = (email) => {
	return User.findOneAndDelete({email: email}).then(result => {
		if(result){
			return true
		}
	})
}

//Enroll
module.exports.enroll = async (data) => {
	const {userId, courseId} = data


	//look for matching document of a user
	const userEnroll = await User.findById(userId).then( (result, err) => {
		if(err){
			return err
		} else {
			// console.log(result)
			result.enrollments.push({courseId: courseId})

			return result.save().then( result => {
				return true
			})
		}

	})

	//look for matching document of a user
	const courseEnroll = await Course.findById(courseId).then( (result, err) => {
		if(err){
			return err
		} else {

			result.enrollees.push({userId: userId})

			return result.save().then( result => {
				return true
			})
		}
	})

	//to return only one value for the function enroll

	if(userEnroll && courseEnroll){
		return true
	} else {
		return false
	}
}
