
const express = require('express');
//Create routes

//Router() handles the requests
const router = express.Router();

//User Model
// const User = require('./../models/User');

//syntax: router.HTTPmethod('uri', <request listener>)
// router.get('/', (req, res) => {
// 	res.send("Hello from userRoutes")
// })

const userController = require('./../controllers/userControllers');

const auth = require('./../auth');

router.post('/email-exists', (req, res) => {

	/* **moved to userControllers.js**
	let email = req.body.email
	find the matching docment in the database using email
		by using Model method
	User.findOne({email: req.body.email}).then( (result, error) => {
	console.log(result)//null
		if(result != null){
			res.send(false)
		}else{
			if(result == null){
				res.send(true)
			}else{
				res.send(error)
			}
		}
	})
	send back the response to the client
	*/

	//invoke the function here
	userController.checkEmail(req.body.email).then( result => res.send(result))

});

//register route
router.post('/register', (req, res) => {
	//expecting data/object coming from request body

	userController.register(req.body).then(result => res.send(result))
})



//Activity - GET all users
	//create a route to get all users
	// endpoint "/"
	//method get
	//model method find()
	//output should be array of users

router.get('/', (req,res) => {
    userController.getAllUsers().then(result => res.send(result))
})


//login
router.post('/login', (req, res) => {
	userController.login(req.body).then( result => res.send(result))
})

//get token from header
router.get('/details', auth.verify,(req, res) => {
	// console.log(req.headers.authorization)
	let userData = auth.decode(req.headers.authorization)

	userController.getUserProfile(userData).then(result => res.send(result))

})

//edit user information
router.put('/:userId/edit', (req, res) => {
	// console.log(req.params)
	// console.log(req.body)
	const userId = req.params.userId

	userController.editProfile(userId, req.body).then(result => res.send(result))
})

// http://localhost:3001/api/users/edit
router.put('/edit', auth.verify, (req, res)=> {
	// console.log(req.params)
	// console.log(req.body)
	// const userId = req.params.userId

	// userController.editProfile(userId, req.body).then(result => res.send(result))
	// console.log(req.headers.authorization)
	let userId = auth.decode(req.headers.authorization).id

	userController.editUser(userId, req.body).then(result => res.send(result))
})

//mini activity
	// create a route to update user details with the following:
		// endpoint = "/edit-profile"
		// function name: editDetails
		// method: put
		// use email as filter to findOneAndUpdate() method


router.put('/edit-profile', (req, res)=> {


userController.editDetails(req.body).then(result => res.send(result))
})


//mini activity
	// create a route to delete a specific user with the following:
		// endpoint = "/:userId/delete"
		// function name: delete()
		// method: delete
		// use id as filter to findByIdAndDelete method
		// return true if document was successfully deleted

router.delete('/:userId/delete', (req, res) => {
	userController.delete(req.params.userId).then( result => res.send(result))
})

//mini activity
	// create a route to delete a specific user with the following:
		// endpoint = "/delete"
		// function name: deleteUser()
		// method: delete
		// use email as filter to findOneAndDelete method
		// return true if document was successfully deleted
router.delete('/delete', (req, res) => {
	const email = auth.decode(req.headers.authorization).email
	userController.deleteUser(email).then(result => res.send(result))
})


//enrollments
router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		//user id of the logged in user
		userId: auth.decode(req.headers.authorization).id,
		//course id of the course you're enrolling in, to be supplied by user
		courseId: req.body.courseId
	}

	userController.enroll(data).then(result => res.send(result))
})

module.exports = router;


